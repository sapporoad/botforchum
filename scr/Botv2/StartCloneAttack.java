package Botv2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Sapporo on 29.07.15.
 */
public class StartCloneAttack {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите кол-во клонов: ");
        String s = reader.readLine();
        int numbBot = Integer.parseInt(s);
        int i = 1;
        while (i <= numbBot) {
            MultiBot bot = new MultiBot();
            Thread threads = new Thread(bot);
            threads.start();
            i++;
        }
    }
}
