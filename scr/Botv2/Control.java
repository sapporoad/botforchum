package Botv2;

/**
 * Created by Sapporo on 25.07.15.
 */

import java.util.Random;

public class Control implements Runnable {
    private MiniClient connect;
    private Random rnd;
    private String userName;
    private String password;

    public Control() {
        getRandomName();
    }

    public void play(String ip, int port) {
        connect = new MiniClient();
        Thread thread2 = new Thread(connect);
        connect.connectServ(ip, port);
        thread2.start();
    }

    public void login() {
        connect.message(getName() + "@logo@" + getPassword());
    }

    public void refreshList() {
        connect.message(getName() + "@list@01");
    }

    public void sendMessageForAllUsers() {
        connect.message(getName() + "@alls@" + getRandomMess());
    }

    public void sendRandomMessageForAll() {
        connect.message(getName() + "@alls@" + "На моем кубике выпало число: " + getNumbSquare());
    }

    public void userExit() {
        connect.message(getName() + "@exit@01");
        connect.closeSocket();
    }

    public String getRandomMess() {
        String mess = "Привет! Мое имя: " + getName();
        return mess;
    }

    public String getNumbSquare() {
        String mess = " " + (rnd.nextInt(6) + 1);
        return mess;
    }

    public String getName() {
        return userName;
    }

    public String getRandomName() {
        rnd = new Random();
        String un = "bot" + String.valueOf(rnd.nextInt(5000 + 1));
        userName = un;
        return un;
    }

    public String getPassword() {
        password = "123456";
        return password;
    }

    @Override
    public void run() {
        while (true) {

        }
    }
}
