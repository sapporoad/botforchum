package Botv2;

import java.util.Random;

/**
 * Created by Sapporo on 28.07.15.
 */
public class MultiBot implements Runnable {
    private int timeThread;

    public void setTimeThread(int timeThread) {
        this.timeThread = timeThread;
    }

    public int getTimeThread() { //Задаем рандомный перерыв между логинами/отправками сообщений 3-10 сек
        Random randomTime = new Random();
        timeThread = (randomTime.nextInt(7) + 3) * 1000;
        return timeThread;
    }

    public void startMultiBot() {
        Control boter = new Control();
        Thread thread1 = new Thread(boter);
        thread1.start();
        boter.play("52.4.219.204", 8189);
        try {
            thread1.sleep(getTimeThread());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boter.login();
        try {
            thread1.sleep(getTimeThread());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boter.refreshList();
        try {
            thread1.sleep(getTimeThread());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boter.sendMessageForAllUsers();
        try {
            thread1.sleep(getTimeThread());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boter.sendRandomMessageForAll();
        int i = 1;
        while (i <= 10) {
            try {
                thread1.sleep(getTimeThread());//каждые 3-6 сек отправляем месседжи
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boter.sendRandomMessageForAll();
            i++;
        }
        try {
            thread1.sleep(getTimeThread());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boter.userExit();
        try {
            thread1.sleep(getTimeThread());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        startMultiBot();
    }
}
