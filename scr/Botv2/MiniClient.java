package Botv2;

/**
 * Created by Sapporo on 25.07.15.
 */

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class MiniClient implements Runnable {
    private Socket sock;
    private InputStream inStr;
    private OutputStream outStr;
    private Scanner scan;
    private PrintWriter writer;
    private InputStreamReader isr;
    private OutputStreamWriter osr;

    public void connectServ(String ip, int port) {
        try {
            sock = new Socket(ip, port);
            inStr = sock.getInputStream();
            outStr = sock.getOutputStream();
            isr = new InputStreamReader(inStr, "UTF-8");
            osr = new OutputStreamWriter(outStr, "UTF-8");
            scan = new Scanner(isr);
            writer = new PrintWriter(osr, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnect() {
        return sock.isConnected();
    }

    public void closeSocket() {
        try {
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void message(String message) {
        writer.println(message);
    }

    @Override
    public void run() {
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
        }
    }
}
